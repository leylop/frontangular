import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trademarks } from '../Modelo/Trademarks';
import { Product } from '../Modelo/Product';
import { AddProduct } from '../Modelo/AddProduct';
import { Categories } from '../Modelo/Categories';
import { ProductsWithMinimunAlert } from '../Modelo/ProductsWithMinimunAlert';
import { UpdateProduct } from '../Modelo/UpdateProduct';
import { PurcaseOrdersComplete } from '../Modelo/PurcaseOrdersComplete';
import { GetLoadInventory } from '../Modelo/GetLoadInventory';
import{AddInventoryPurchaseOrder} from '../Modelo/AddInventoryPurchaseOrder'



@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  url="/api/v1/Trademarks";
  url1="/api/v1/Products/";
  Urladd ='/api/v1/AddProduct' ;
  urlc="/api/v1/Categories";
  urlUp="/api/v1/UpdateProduct/"
  urlAlert="/api/v1/ProductsWithMinimumAlert";
  urlpc="/api/v1/GetPurcaseOrders";
  urlgli="/api/v1/GetLoadInventory/";
  urlco="/api/v1/AddInventoryPurchaseOrder";
  
 getTrademarks(){
  return this.http.get<Trademarks[]>(this.url);

 }

 getProducts(){
  return this.http.get<Product[]>(this.url1);

}
createProduct(AddProduct: AddProduct){
  return  this.http.post<AddProduct>(this.Urladd, AddProduct);
}
getCategories(){
  return this.http.get<Categories[]>(this.urlc);

 }
 getProductsId(idproduct:number){
  return this.http.get<Product>(this.url1+idproduct);

}
updateProducto(updateProduct:UpdateProduct){
  return this.http.post<Product>(this.urlUp,updateProduct);
}
getMinimun(){
  return this.http.get<ProductsWithMinimunAlert[]>(this.urlAlert);
}

getPurcaseOrdersComplete(){
  return this.http.get<PurcaseOrdersComplete[]>(this.urlpc);
}

GetLoadInventory(id){
  return this.http.get<GetLoadInventory[]>(this.urlgli+id);
}
AddInventoryPurchaseOrder(Addipo:AddInventoryPurchaseOrder){
return  this.http.post<AddInventoryPurchaseOrder>(this.urlco, Addipo);
}


}