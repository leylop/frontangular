import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { InventoryComponent } from '../../inventory/inventory.component';
import {CreateInventoryComponent} from '../../inventory/create-inventory/create-inventory.component';
import {ShowInventoryComponent } from '../../inventory/show-inventory/show-inventory.component';
import {CreateProductComponent } from '../../inventory/create-product/create-product.component';
import {UpdateProductComponent} from '../../inventory/update-product/update-product.component';
import {ShowProductComponent } from '../../inventory/show-product/show-product.component';
import {ShowPurcaseordersComponent } from '../../inventory/show-purcaseorders/show-purcaseorders.component';




export const AdminLayoutRoutes: Routes = [
 
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'inventory',   component: InventoryComponent },
    { path: 'show-inventory',   component: ShowInventoryComponent }, 
    { path: 'create-inventory',   component: CreateInventoryComponent }, 
    { path: 'create-product',   component: CreateProductComponent }, 
    { path: 'show-product',   component: ShowProductComponent },
    { path: 'update-product',   component: UpdateProductComponent },    
    { path: 'show-purcaseorders',   component: ShowPurcaseordersComponent }
  ];
