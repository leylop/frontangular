import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { InventoryComponent } from '../../inventory/inventory.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {CreateInventoryComponent} from '../../inventory/create-inventory/create-inventory.component';
import {ShowInventoryComponent } from '../../inventory/show-inventory/show-inventory.component';
import {CreateProductComponent} from '../../inventory/create-product/create-product.component';
import {ShowProductComponent} from '../../inventory/show-product/show-product.component';
import {UpdateProductComponent} from '../../inventory/update-product/update-product.component';
import {MatDialogModule} from "@angular/material/dialog";
import { ShowPurcaseordersComponent } from 'app/inventory/show-purcaseorders/show-purcaseorders.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatMenuModule,
    MatListModule,
    MatDatepickerModule,
    MatDialogModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    InventoryComponent,
    UpgradeComponent,
    ShowInventoryComponent,
    CreateInventoryComponent,
    CreateProductComponent,
    ShowProductComponent,
    UpdateProductComponent,
    ShowPurcaseordersComponent,
  ]
})

export class AdminLayoutModule {}
