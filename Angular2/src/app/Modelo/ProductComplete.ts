import { Product } from './Product';
import { Stock } from './Stock';
import { Trademarks } from './Trademarks';
export class ProductComplete {
    Product: Product;
    Trademark: Trademarks;
    Stock: Stock;
}