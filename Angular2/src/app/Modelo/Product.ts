export class Product {
    actualamount:number;
    trademarks_idtrademark: number;
    categories_idcategory: number;
    description: String;
    minimuamount: number;    
    reference: String;
    idproduct: number;
    supplierreference: String;
    product: String;
    supplierdescription: String;
    saleprice: number;
    purchaseprice: number;
    state: boolean;
    weight: number;
    localidentifier: String;
    localdescription: String;    
    
}