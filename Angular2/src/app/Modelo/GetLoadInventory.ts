import { Product } from './Product';
import { Stock } from './Stock';
import { Trademarks } from './Trademarks';
import { Categories } from './Categories';
import { ProductOrder } from './ProductOrder';
import { Oder } from './Oder';

export class GetLoadInventory {
    Oder: Oder;
    Product: Product;
    ProductOrder:ProductOrder; 
    Trademark: Trademarks;
    ProductAddStock: null;
    Category: null;
}