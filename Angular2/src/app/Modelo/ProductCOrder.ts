import { Product } from './Product';
import { Stock } from './Stock';
import { Trademarks } from './Trademarks';
import { Categories } from './Categories';
import { ProductOrder } from './ProductOrder';

export class ProductCOrder {
    Product: Product;
    Trademark: Trademarks;
    ProductAddStock: Stock;
    Category: Categories;
    ProductOrder: ProductOrder;
}