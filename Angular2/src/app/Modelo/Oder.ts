export class Oder {
    idpurchaseorder: number;
    suppliers_idsupplier: number;
    datepurchase: Date;
    description: String;
    datestrockentry: Date;
    descriptionStockEntry: String;
    stateStockEntry: String;
    state: boolean;
}