export class Suppliers {
    idSupplier: number;
    tradeName: String;
    typeId: String;
    numberId: String;
    description: String;
    state: String;
}