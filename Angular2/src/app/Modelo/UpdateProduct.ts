export class UpdateProduct {
    idproduct:number
    categories_idcategory: number;
    minimuamount: number;
    actualamount: number;
    supplierreference: String;
    product: String;
    supplierdescription: String;
    saleprice: number;
    purchaseprice: number;
    trademarks_idtrademark: number;
    state: boolean;
    weight: number;
    localidentifier: String;
    localdescription: String;
}