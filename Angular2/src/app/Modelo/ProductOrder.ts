export class ProductOrder {
    id: number;
    purchaseorders_idpurchaseorder: number;
    products_idproduct: number;
    orderunits: number;
}