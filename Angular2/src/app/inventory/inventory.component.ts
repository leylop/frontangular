import { Component, OnInit,ViewChild,Input, NgModule } from '@angular/core';
import { Router } from '@angular/router';
/*import {CreateInventoryComponent} from '../inventory/create-inventory/create-inventory.component';
import {ShowInventoryComponent } from '../inventory/list-inventory/list-inventory.component';*/

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})

export class InventoryComponent implements OnInit {
 /*@ViewChild('child1')childOne:CreateInventoryComponent;
 @ViewChild('child2')childTwo:ShowInventoryComponent;*/
  
 constructor(private router:Router) {}
 
 menu(val){   
  switch(val) { 
    case val=1: { 
      this.router.navigate(['inventory'])
      break; 
    } 
    case val=2: { 
      this.router.navigate(['create-product'])
      break; 
    } 
    case val=3: { 
      this.router.navigate(['show-product'])
      break; 
    } 
    case val=4: { 
      this.router.navigate(['show-purcaseorders'])
      break; 
    } 
    case val=5: { 
      this.router.navigate(['show-inventory'])
      break; 
    }
    default: {          
      break; 
    } 
}

}

 ngOnInit() {
}

  
}
