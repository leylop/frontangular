import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'app/Service/service.service';
import { HttpClient } from '@angular/common/http';
import { PurcaseOrdersComplete } from '../../Modelo/PurcaseOrdersComplete';
import { GetLoadInventory } from '../../Modelo/GetLoadInventory';
import {AddInventoryPurchaseOrder} from '../../Modelo/AddInventoryPurchaseorder'
import { Oder } from '../../Modelo/Oder';
declare var $: any;

@Component({
  selector: 'app-show-purcaseorders',
  templateUrl: './show-purcaseorders.component.html',
  styleUrls: ['./show-purcaseorders.component.css']
})

export class ShowPurcaseordersComponent implements OnInit {
  purcaseOrdersComplete:PurcaseOrdersComplete[];
  getLoadinv:GetLoadInventory[];

  constructor(private router:Router, private service:ServiceService, private http: HttpClient) {
   
  }
  menu(val){   
    switch(val) { 
      case val=1: { 
        this.router.navigate(['inventory'])
        break; 
      } 
      case val=2: { 
        this.router.navigate(['create-product'])
        break; 
      } 
      case val=3: { 
        this.router.navigate(['show-product'])
        break; 
      } 
      case val=4: { 
        this.router.navigate(['show-purcaseorders'])
        break; 
      } 
      case val=5: { 
        this.router.navigate(['show-inventory'])
        break; 
      }
      default: {          
        break; 
      } 
  }
  
  }
 
  ngOnInit(): void {  
    this.service.getPurcaseOrdersComplete()
    .subscribe(data =>{
      this.purcaseOrdersComplete=data;      
    })
    //this.showOrderDetail();
  
  }

  showOrderDetail(id){
    this.service.GetLoadInventory(id)
    .subscribe(data =>{
      $('#cargarOrden').show();
      this.getLoadinv=data;
     var respuesta=JSON.stringify(data); 
   
    
    })
  }

  Addipo: AddInventoryPurchaseOrder = new AddInventoryPurchaseOrder();
  SendOrder(){
    this.service.AddInventoryPurchaseOrder(this.Addipo)
    .subscribe(data => { 
      var from="top";
      var align="center";
      this.showNotification(from,align); 

    });
    
  }

  showNotification(from, align){
    const type = ['','info','success','warning','succes'];

   // const color =Math.floor((Math.random() * 3) + 1);

    $.notify({
        icon: "notifications",
        message: "Orden cargada con exito!"

    },{
        type: 'success',
        timer: 1000,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button color="green" aria-hidden="true" class="close mat-button"   data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="5" aria-valuemax="150" style="width:10%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '<div> </div>'
    });
    this.router.navigate(['show-purcaseorders']);
    $('#cargarOrden').hidden();
}

  }
  
