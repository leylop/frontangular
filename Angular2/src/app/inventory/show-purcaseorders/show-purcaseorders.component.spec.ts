import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPurcaseordersComponent } from './show-purcaseorders.component';

describe('ShowPurcaseordersComponent', () => {
  let component: ShowPurcaseordersComponent;
  let fixture: ComponentFixture<ShowPurcaseordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPurcaseordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPurcaseordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
