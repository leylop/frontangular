import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../Modelo/product';
//import { ProductByFilter } from '../../Modelo/ProductByFilter';
import { ServiceService } from '../../Service/service.service';
import { ResponseOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.css']
})
export class ShowProductComponent implements OnInit {
  product:Product[];
 
  constructor(private router:Router, private service:ServiceService, private http: HttpClient) {
   
   }
  
   menu(val){   
    switch(val) { 
      case val=1: { 
        this.router.navigate(['inventory'])
        break; 
      } 
      case val=2: { 
        this.router.navigate(['create-product'])
        break; 
      } 
      case val=3: { 
        this.router.navigate(['show-product'])
        break; 
      } 
      case val=4: { 
        this.router.navigate(['show-purcaseorders'])
        break; 
      } 
      case val=5: { 
        this.router.navigate(['show-inventory'])
        break; 
      }
      default: {          
        break; 
      } 
  }
  
  }
 
  ngOnInit(): void {  
    this.service.getProducts()
    .subscribe(data =>{
      this.product=data;      
    })
  }
  Editar(product:Product):void{    

    localStorage.setItem("id",product.idproduct.toString());   
    this.router.navigate(['update-product'])
  }
  
}
