import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-create-inventory',
  templateUrl: './create-inventory.component.html',
  styleUrls: ['./create-inventory.component.css']
})
export class CreateInventoryComponent implements OnInit {
  createInventory:FormGroup;
  constructor(private router:Router, private formBuilder:FormBuilder) { }
  
  menu(val){   
    switch(val) { 
      case val=1: { 
        this.router.navigate(['inventory'])
        break; 
      } 
      case val=2: { 
        this.router.navigate(['create-product'])
        break; 
      } 
      case val=3: { 
        this.router.navigate(['show-product'])
        break; 
      } 
      case val=4: { 
        this.router.navigate(['show-purcaseorders'])
        break; 
      } 
      case val=5: { 
        this.router.navigate(['show-inventory'])
        break; 
      }
      default: {          
        break; 
      } 
  }
  
  }
  ngOnInit(): void {

  

  }

}
