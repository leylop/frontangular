import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../Service/service.service';
import { Product } from '../../Modelo/product';
import { Trademarks } from '../../Modelo/Trademarks';
import { Categories } from '../../Modelo/Categories';
//import { AddProduct }from '../../Modelo/AddProduct';
import { UpdateProduct }from '../../Modelo/UpdateProduct';
declare var $: any;

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  trademarks:Trademarks[];
  categories:Categories[];
  product: Product = new Product();
  updateProduct: UpdateProduct = new UpdateProduct(); 
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    this.Editar();
    this.service.getTrademarks()
    .subscribe(data =>{
      this.trademarks=data;     
    })

    this.service.getCategories()
    .subscribe(data =>{
      this.categories=data;     
    })
   
  }

  menu(val){   
    switch(val) { 
      case val=1: { 
        this.router.navigate(['inventory'])
        break; 
      } 
      case val=2: { 
        this.router.navigate(['create-product'])
        break; 
      } 
      case val=3: { 
        this.router.navigate(['show-product'])
        break; 
      } 
      case val=4: { 
        this.router.navigate(['show-purcaseorders'])
        break; 
      } 
      case val=5: { 
        this.router.navigate(['show-inventory'])
        break; 
      }
      default: {          
        break; 
      } 
  }
  
  }

  Editar(){    
    let idproduct=localStorage.getItem("id");
   // alert(idproduct);
    this.service.getProductsId(+idproduct)
    .subscribe(data=>{
      this.product=data;
    })
  }
 Actualizar(id){
  this.updateProduct.idproduct=id; 
  this.service.updateProducto(this.updateProduct)
    .subscribe(data => { 
      var from="top";
      var align="center";
      this.showNotification(from,align);
    });
  }

  showNotification(from, align){
    const type = ['','info','success','warning','succes'];

   // const color =Math.floor((Math.random() * 3) + 1);

    $.notify({
        icon: "notifications",
        message: "Producto actualizado con exito!"

    },{
        type: 'success',
        timer: 1000,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button color="red" aria-hidden="true" class="close mat-button"   data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="5" aria-valuemax="150" style="width:10%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '<div> </div>'
    });
    this.router.navigate(['show-product']) 
     
    
}
   

}
